# SiteChecker


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'site_checker'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install site_checker


## Development

After checking out the repo, run `bin/setup` to install dependencies. 
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. 
